#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <time.h>
#include <pthread.h>

#include "packet.h"

#define MAXLINE 80
#define PORT 8000
#define SA struct sockaddr

#define MESSAGE_SIZE 10
#define N_TOPICS 8

unsigned char connect_msg[] = {0x10, 0x10, 0x00, 0x04, 0x4d, 0x51, 0x54, 0x54,
    0x05, 0x02, 0x00, 0x3c, 0x03, 0x21, 0x00, 0x14, 0x00, 0x00};
unsigned char disconnect_msg[] = { 0xe0, 0x00 };


// unsigned char publish_msg[] = {0x30, 0x0d, 0x00, 0x03, 0x31, 0x30,
// 0x30, 0x00, 0x74, 0x72, 0x61, 0x6c, 0x61, 0x6c, 0x61};

// unsigned char subscribe_msg[] = { 0x82, 0x11, 0x00, 0x01, 0x00,
// 0x00, 0x05, 0x61, 0x62, 0x63, 0x64, 0x65, 0x00, 0x00, 0x03,
// 0x78, 0x79, 0x7a, 0x00};

char *topics[] = { "abc", "bcd", "cde", "def", "efg", "123", "456", "789", 0};

char *random_string(char *c, int len) {
  c[len] = 0;
  for (int i = 0; i < len; i++)
    c[i] = (char) ((random() % 26) + 97); // [a-z]
  return c;
}

void *write_routine(void *arg) {
  int sockfd = * (int*) arg;
  int topic_i;
  char message[MESSAGE_SIZE];
  uint8_t *packet;
  uint64_t packet_size;

  while (1) {
    pthread_testcancel();

    topic_i = rand() % N_TOPICS;
    packet = mk_publish_packet(topics[topic_i],
                               random_string(message, MESSAGE_SIZE),
                               &packet_size);
    write(sockfd, packet, packet_size);
    free(packet);
    usleep(10);
  }
  return NULL;
}

int main(int argc, char ** argv)
{
    int sockfd;
    struct sockaddr_in servaddr;
    char buff[MAXLINE];
    ssize_t read_size;
    uint64_t packet_size;

    pthread_t tid;
    uint8_t *packet;

    if (argc != 2) {
        fprintf(stderr,"Uso: %s <Porta>\n", argv[0]);
        fprintf(stderr,"Vai rodar um cliente de teste na porta <Porta> TCP\n");
        exit(1);
    }

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd == -1) {
        fprintf(stderr, "socket creation failed...\n");
        exit(0);
    }
    bzero(&servaddr, sizeof(servaddr));

    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = inet_addr("127.0.0.1");
    servaddr.sin_port = htons(atoi(argv[1]));

    // connect the client socket to server socket
    if (connect(sockfd, (SA*)&servaddr, sizeof(servaddr)) != 0) {
        fprintf(stderr, "connection with the server failed...\n");
        exit(0);
    }

    srand((unsigned int)time(NULL));

    /* Connect */
    write(sockfd, connect_msg, sizeof(connect_msg));

    read(sockfd, buff, MAXLINE);

    packet = mk_subscribe_packet(topics, &packet_size);
    write(sockfd, packet, packet_size);
    read(sockfd, buff, MAXLINE);
    free(packet);

    pthread_create(&tid, NULL, write_routine, (void*)&sockfd);

    while ((read_size = read(sockfd, buff, MAXLINE)) > 0) {
      printf("recebi uma mensagem.\n");
    }

    pthread_cancel(tid);
    close(sockfd);

    /*     printf("-----------\n"); */
    /*     for (ssize_t i = 0; i < size; i++) */
    /*       printf("%02x ", (unsigned char) buff[i]); */
    /*     printf("\n-----------\n"); */
}
