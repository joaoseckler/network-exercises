#!/bin/bash

if [ $# -ne 3 ]; then
  echo traffic: passe o tempo de espera como primeiro argumento e o
  echo nome do arquivo como segundo argumento, por favor
  echo e o terceiro a porta a ser analisada
  exit 1
fi

sum=0

rm -f $$.txt

sudo tcpdump -i lo -n port $3 -v > $$.1 & sleep $1; sudo pkill tcpdump
grep --line-buffered -o -P '(?<=length )\d*(?=\))' $$.1 >> $$.2

while read num; do ((sum += $num)); done < $$.2

echo sum: $sum >> "$2"

rm $$.{1,2}
