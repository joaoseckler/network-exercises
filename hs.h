#include <stdint.h>
#include <pthread.h>

#define HS_SIZE 1024
#define HS_VALUES_INITIAL_SIZE 8

/* This implements a "subscription hash table": a hash table whose
 * values are lists of integers. Inserting into this hash table means
 * including one integer to that list (subscribing), but getting from it
 * means getting the entire list. */

typedef struct node {
  struct node * next;
  char *key;
  int *values;
  unsigned int allocated;
  unsigned int n;
} node;

typedef struct {
    // The position of this next in the struct must match the position
    // of next in the struct node (pointers will be cast from here to
    // there)
    node *next;
    pthread_mutex_t mutex;
} head;

typedef head* hs_t;

unsigned int hs_hash(unsigned char *key);
hs_t hs_init();
void hs_destroy(hs_t hs);
void hs_insert(hs_t hs, char *key, int value);

/* Callers of the following function must unlock the mutex after using
 * the retrieved data */
int *hs_get(hs_t hs, char *key, pthread_mutex_t **mutex, uint64_t *n);
void hs_delete(hs_t hs, char *key, int value);
void hs_delete_all(hs_t hs, int value);

