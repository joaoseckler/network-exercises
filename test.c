#include "packet.h"
#include "hs.h"
#include <pthread.h>
#include <string.h>

#define fazstr(dest, orig) str=malloc(strlen(orig) + 1); strcpy(dest, orig);

hs_t hs;

int main() {
    pthread_mutex_t *m;
    uint64_t size;
    char c1[] = "TRalalala";
    char c2[] = "TRalalal2";
    char c3[] = "TRalalal3";
    char c4[] = "TRalalal4";
    int *values;

    hs = hs_init();


    char *str;

    fazstr(str, c1)
    hs_insert(hs, str, 1);
    fazstr(str, c1)
    hs_insert(hs, str, 2);
    fazstr(str, c1)
    hs_insert(hs, str, 3);
    fazstr(str, c1)
    hs_insert(hs, str, 4);

    printf("Teste de inserção. Deve vir 1, 2, 3, 4\n");
    printf("%d\n", hs_get(hs, c1, &m, &size)[0]);
    pthread_mutex_unlock(m);
    printf("%d\n", hs_get(hs, c1, &m, &size)[1]);
    pthread_mutex_unlock(m);
    printf("%d\n", hs_get(hs, c1, &m, &size)[2]);
    pthread_mutex_unlock(m);
    printf("%d\n", hs_get(hs, c1, &m, &size)[3]);
    pthread_mutex_unlock(m);

    hs_delete(hs, c1, 1);
    hs_delete(hs, c1, 2);
    hs_delete(hs, c1, 3);
    hs_delete(hs, c1, 4);
    printf("--------\n");

    printf("Teste de deleção. Deve vir 4 nulls depois mais 4\n");
    printf("%p\n", (void *) hs_get(hs, c2, &m, &size));
    pthread_mutex_unlock(m);
    printf("%p\n", (void *) hs_get(hs, c1, &m, &size));
    pthread_mutex_unlock(m);
    printf("%p\n", (void *) hs_get(hs, c1, &m, &size));
    pthread_mutex_unlock(m);
    printf("%p\n", (void *) hs_get(hs, c1, &m, &size));
    pthread_mutex_unlock(m);

    fazstr(str, c1)
    hs_insert(hs, str, 5);
    fazstr(str, c2)
    hs_insert(hs, str, 5);
    fazstr(str, c3)
    hs_insert(hs, str, 5);
    fazstr(str, c4)
    hs_insert(hs, str, 5);
    printf("--------\n");

    hs_delete_all(hs, 5);
    printf("%p\n", (void *) hs_get(hs, c1, &m, &size));
    pthread_mutex_unlock(m);
    printf("%p\n", (void *) hs_get(hs, c2, &m, &size));
    pthread_mutex_unlock(m);
    printf("%p\n", (void *) hs_get(hs, c3, &m, &size));
    pthread_mutex_unlock(m);
    printf("%p\n", (void *) hs_get(hs, c4, &m, &size));
    pthread_mutex_unlock(m);
    printf("--------\n");

    printf("Teste de realocação. Não deve vir nada\n");
    for (int i = 0; i < 8*HS_VALUES_INITIAL_SIZE + 2; i++) {
      fazstr(str, c1)
      hs_insert(hs, str, i);
    }
    values = hs_get(hs, c1, &m, &size);
    if (size != 8*HS_VALUES_INITIAL_SIZE + 2)
      printf("Ops! size devia ser %d mas é %lu\n", 8*HS_VALUES_INITIAL_SIZE + 2, size);
    for (int i = 0; i < 8*HS_VALUES_INITIAL_SIZE + 2; i++) {
      if (values[i] != i)
        printf("Ops! values[%d] devia ser %d mas é %d\n", i, i, values[i]);
    }
    pthread_mutex_unlock(m);

    for (int i = 0; i < 8*HS_VALUES_INITIAL_SIZE + 2; i++)
      hs_delete(hs, c1, i);
    printf("--------\n");

    printf("Teste de inserção repetida. Não deve vir nada.\n");
    fazstr(str, c1)
    hs_insert(hs, str, 5);
    fazstr(str, c1)
    hs_insert(hs, str, 5);
    fazstr(str, c1)
    hs_insert(hs, str, 5);
    fazstr(str, c1)
    hs_insert(hs, str, 5);

    values = hs_get(hs, c1, &m, &size);
    if (size != 1)
      printf("Ops! size devia ser 1 mas é %lu\n", size);
    pthread_mutex_unlock(m);
    hs_delete(hs, c1, 5);
    printf("--------\n");

    printf("Teste de destruição. Não deve vir nada. Teste com valgrind\n");
    printf("--------\n");
    for (int i = 0; i < 8*HS_VALUES_INITIAL_SIZE + 2; i++) {
      fazstr(str, c1)
      hs_insert(hs, str, i);
    }

    fazstr(str, c1)
    hs_insert(hs, str, 5);
    fazstr(str, c2)
    hs_insert(hs, str, 5);
    fazstr(str, c3)
    hs_insert(hs, str, 5);
    fazstr(str, c4)
    hs_insert(hs, str, 5);

    hs_destroy(hs);

    return 0;
}
