/* Por Prof. Daniel Batista <batista@ime.usp.br>
 * Em 4/4/2021
 *
 * Um código simples de um servidor de eco a ser usado como base para
 * o EP1. Ele recebe uma linha de um cliente e devolve a mesma linha.
 * Teste ele assim depois de compilar:
 *
 * ./ep1-servidor-exemplo 8000
 *
 * Com este comando o servidor ficará escutando por conexões na porta
 * 8000 TCP (Se você quiser fazer o servidor escutar em uma porta
 * menor que 1024 você precisará ser root ou ter as permissões
 * necessáfias para rodar o código com 'sudo').
 *
 * Depois conecte no servidor via telnet. Rode em outro terminal:
 *
 * telnet 127.0.0.1 8000
 *
 * Escreva sequências de caracteres seguidas de ENTER. Você verá que o
 * telnet exibe a mesma linha em seguida. Esta repetição da linha é
 * enviada pelo servidor. O servidor também exibe no terminal onde ele
 * estiver rodando as linhas enviadas pelos clientes.
 *
 * Obs.: Você pode conectar no servidor remotamente também. Basta
 * saber o endereço IP remoto da máquina onde o servidor está rodando
 * e não pode haver nenhum firewall no meio do caminho bloqueando
 * conexões na porta escolhida.
 */

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <time.h>
#include <unistd.h>
#include <pthread.h>
#include <signal.h>

#include "packet.h"
#include "hs.h"

#define LISTENQ 1
#define MAXDATASIZE 100
#define MAXLINE 4096
#define INITIAL_ACTIVE_CONN_SIZE 16

hs_t hs;
int listenfd;

/* List of active connections */
int *active_conn;
int active_conn_size;
pthread_mutex_t active_conn_m = PTHREAD_MUTEX_INITIALIZER;
pthread_attr_t attr;

uint8_t connack[] = {0x20, 0x03, 0x00, 0x00, 0x00};
uint8_t pingresp[] = {0xd0, 0x00};

void cleanup() {
    printf("\nEncerrando...\n");
    pthread_mutex_lock(&active_conn_m);
    for (int i = 0; i < active_conn_size; i++) {
        if (active_conn[i]) {
            close(i);
        }
    }
    /* pthread_mutex_unlock(&active_conn_m); */

    pthread_mutex_destroy(&active_conn_m);
    pthread_attr_destroy(&attr);
    hs_destroy(hs);
    close(listenfd);
    free(active_conn);
    exit(0);
}

void *thread_main_routine(void *arg) {
    char *topic, *message, **topics;
    uint8_t recvline[MAXLINE + 1], *packet;
    uint16_t identifier;
    int connfd = *((int*) arg), *fdlist;
    ssize_t read_size;
    uint64_t fdlist_n, packet_size;
    pthread_mutex_t *mutex;

    printf("[Uma conexão aberta na thread %lu]\n", pthread_self());
    /* Agora pode ler do socket e escrever no socket. Isto tem
     * que ser feito em sincronia com o cliente. Não faz sentido
     * ler sem ter o que ler. Ou seja, neste caso está sendo
     * considerado que o cliente vai enviar algo para o servidor.
     * O servidor vai processar o que tiver sido enviado e vai
     * enviar uma resposta para o cliente (Que precisará estar
     * esperando por esta resposta)
     */

    /* ========================================================= */
    /* ========================================================= */
    /*                         EP1 INÍCIO                        */
    /* ========================================================= */
    /* ========================================================= */

    while ((read_size=read(connfd, recvline, MAXLINE)) > 0) {
        switch (recvline[0] >> 4) {
            case CONNECT:
                write(connfd, connack, sizeof(connack));
                break;
            case PUBLISH:
                parse_publish_packet(recvline, &topic, &message);
                printf("PUBLISH: topic: %s, message: %s\n", topic, message);

                fdlist = hs_get(hs, topic, &mutex, &fdlist_n);
                packet = mk_publish_packet(topic, message, &packet_size);
                for (unsigned int i = 0; i < fdlist_n; i++) {
                    printf("  Enviando a mensagem para %d\n", fdlist[i]);
                    write(fdlist[i], packet, packet_size);
                }
                pthread_mutex_unlock(mutex);
                free(message);
                free(topic);
                free(packet);
                break;

            case SUBSCRIBE:
                topics = parse_subscribe_packet(recvline, &identifier);
                printf("SUBSCRIBE:\n");
                for (unsigned int i = 0; topics[i] != NULL; i++) {
                    printf("  inscrevendo %d no tópico %s\n", connfd, topics[i]);
                    hs_insert(hs, topics[i], connfd);
                }
                packet = mk_suback_packet(identifier, &packet_size);
                write(connfd, packet, packet_size);

                free(packet);
                free(topics);
                break;
            case PINGREQ:
                write(connfd, pingresp, sizeof(pingresp));
                break;
            case DISCONNECT:
                break;

        }
    }
    /* ========================================================= */
    /* ========================================================= */
    /*                         EP1 FIM                           */
    /* ========================================================= */
    /* ========================================================= */

    hs_delete_all(hs, connfd);
    free(arg);

    pthread_mutex_lock(&active_conn_m);
    if (active_conn[connfd])
      close(connfd);
    active_conn[connfd] = 0;
    pthread_mutex_unlock(&active_conn_m);

    printf("[Conexão da thread %lu fechada]\n", pthread_self());

    pthread_exit(NULL);
}

int main(int argc, char **argv) {
    /* Os sockets. Um que será o socket que vai escutar pelas conexões
     * e o outro que vai ser o socket específico de cada conexão */
    int connfd, *arg;
    /* Informações sobre o socket (endereço e porta) ficam nesta struct */
    struct sockaddr_in servaddr;
    /* Armazena linhas recebidas do cliente */
    /* Armazena o tamanho da string lida do cliente */
    pthread_t thread_id;
    struct sigaction new_action;

    if (argc != 2) {
        fprintf(stderr,"Uso: %s <Porta>\n", argv[0]);
        fprintf(stderr,"Vai rodar um servidor de echo na porta <Porta> TCP\n");
        exit(1);
    }

    /* Criação de um socket. É como se fosse um descritor de arquivo.
     * É possível fazer operações como read, write e close. Neste caso o
     * socket criado é um socket IPv4 (por causa do AF_INET), que vai
     * usar TCP (por causa do SOCK_STREAM), já que o MQTT funciona sobre
     * TCP, e será usado para uma aplicação convencional sobre a Internet
     * (por causa do número 0) */
    if ((listenfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
        perror("socket :(\n");
        exit(2);
    }


    /* Agora é necessário informar os endereços associados a este
     * socket. É necessário informar o endereço / interface e a porta,
     * pois mais adiante o socket ficará esperando conexões nesta porta
     * e neste(s) endereços. Para isso é necessário preencher a struct
     * servaddr. É necessário colocar lá o tipo de socket (No nosso
     * caso AF_INET porque é IPv4), em qual endereço / interface serão
     * esperadas conexões (Neste caso em qualquer uma -- INADDR_ANY) e
     * qual a porta. Neste caso será a porta que foi passada como
     * argumento no shell (atoi(argv[1]))
     */
    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family      = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port        = htons(atoi(argv[1]));
    if (bind(listenfd, (struct sockaddr *)&servaddr, sizeof(servaddr)) == -1) {
        perror("bind :(\n");
        exit(3);
    }

    /* Como este código é o código de um servidor, o socket será um
     * socket passivo. Para isto é necessário chamar a função listen
     * que define que este é um socket de servidor que ficará esperando
     * por conexões nos endereços definidos na função bind. */
    if (listen(listenfd, LISTENQ) == -1) {
        perror("listen :(\n");
        exit(4);
    }

    printf("[Servidor no ar. Aguardando conexões na porta %s]\n",argv[1]);
    printf("[Para finalizar, pressione CTRL+c ou rode um kill ou killall]\n");

    /* Inicializa estrutura de dados, variável global */
    hs = hs_init();

    /* Captura do SIGINT */
    new_action.sa_handler = cleanup;
    sigemptyset(&new_action.sa_mask);
    new_action.sa_flags = 0;
    sigaction(SIGINT, &new_action, NULL);

    /* Threads em modo detached evitam vazamento de memória */
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);

    /* Mantém pid das threads*/
    active_conn_size = INITIAL_ACTIVE_CONN_SIZE;
    active_conn = calloc(active_conn_size, sizeof(pthread_t));

    /* O servidor no final das contas é um loop infinito de espera por
     * conexões e processamento de cada uma individualmente */
    for (;;) {
        /* O socket inicial que foi criado é o socket que vai aguardar
         * pela conexão na porta especificada. Mas pode ser que existam
         * diversos clientes conectando no servidor. Por isso deve-se
         * utilizar a função accept. Esta função vai retirar uma conexão
         * da fila de conexões que foram aceitas no socket listenfd e
         * vai criar um socket específico para esta conexão. O descritor
         * deste novo socket é o retorno da função accept. */
        if ((connfd = accept(listenfd, (struct sockaddr *) NULL, NULL)) == -1
             && errno != EINTR) {
            perror("accept :(\n");
            exit(5);
        }
        /* Agora o servidor precisa tratar este cliente de forma
         * separada. Para isto é criado um processo filho usando a
         * função fork. O processo vai ser uma cópia deste. Depois da
         * função fork, os dois processos (pai e filho) estarão no mesmo
         * ponto do código, mas cada um terá um PID diferente. Assim é
         * possível diferenciar o que cada processo terá que fazer. O
         * filho tem que processar a requisição do cliente. O pai tem
         * que voltar no loop para continuar aceitando novas conexões.
         * Se o retorno da função fork for zero, é porque está no
         * processo filho. */
        arg = malloc(sizeof(int));
        *arg = connfd;

        while (active_conn_size <= connfd) {
            active_conn_size *= 2;
            active_conn = realloc(active_conn,
                                  active_conn_size * sizeof(pthread_t));
            for (int i = active_conn_size/2; i < active_conn_size; i++)
                active_conn[i] = 0;
        }

        pthread_mutex_lock(&active_conn_m);
        pthread_create(&thread_id, &attr, thread_main_routine, (void *) arg);
        active_conn[connfd] = thread_id;
        pthread_mutex_unlock(&active_conn_m);
    }
    exit(0);
}

