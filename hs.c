#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "hs.h"

unsigned int hs_hash(unsigned char *key) {
    unsigned int h = 5831;
    for (unsigned int i = 0; key[i] != '\0'; i++)
        h = ((h * 33) ^ key[i]) % HS_SIZE;
    return h;
}

hs_t hs_init() {
    hs_t hs = malloc(sizeof(head) * HS_SIZE);
    memset(hs, 0, sizeof(head) * HS_SIZE);
    return hs;
}

void hs_destroy(hs_t hs) {
    node *p, *next;

    for (int h = 0; h < HS_SIZE; h++) {
        p = (node *) &hs[h];
        next = p->next;

        while ((p = next)) {
            free(p->values);
            free(p->key);
            next = p->next;
            free(p);
        }
    }
    free(hs);
}

void hs_insert(hs_t hs, char *key, int value) {
    hs = hs + hs_hash((unsigned char*) key);
    node *p = (node *) hs;

    pthread_mutex_lock(&hs->mutex);
    while (p->next) {
        p = p->next;
        if (strcmp(p->key, key) == 0) {
            free(key);
            // check if the value is already there
            for (unsigned int i = 0; i < p->n; i++) {
                if (p->values[i] == value) {
                    pthread_mutex_unlock(&hs->mutex);
                    return;
                }
            }

            // if not, append the value
            if (p->n == p->allocated) {
                p->allocated *= 2;
                p->values = reallocarray(p->values,
                                         p->allocated,
                                         sizeof(int));
            }
            p->values[p->n] = value;
            p->n++;
            pthread_mutex_unlock(&hs->mutex);
            return;
        }
    };

    p->next = malloc(sizeof(node));
    p = p->next;
    p->key = key;
    p->values = calloc(HS_VALUES_INITIAL_SIZE, sizeof(int));
    p->allocated = HS_VALUES_INITIAL_SIZE;
    p->values[0] = value;
    p->n = 1;
    p->next = NULL;
    pthread_mutex_unlock(&hs->mutex);
}

int *hs_get(hs_t hs, char *key, pthread_mutex_t **mutex, uint64_t *n) {
    hs = hs + hs_hash((unsigned char*) key);
    node *p = (node *) hs;
    *mutex = &hs->mutex;
    pthread_mutex_lock(&hs->mutex);

    while ((p = p->next))
        if (strcmp(p->key, key) == 0) {
            *n = p->n;
            return p->values;
        }
    *n = 0;
    return NULL;
}
void hs_delete(hs_t hs, char *key, int value) {
    node *prev, *p;
    hs = hs + hs_hash((unsigned char*) key);
    prev = p = (node*) hs;
    unsigned int i;

    pthread_mutex_lock(&hs->mutex);
    while ((p = p->next)) {
        if (strcmp(p->key, key) == 0) {

            for (i = 0; i < p->n && p->values[i] != value; i++);
            if (p->values[i] == value) {
                p->n--;
                if (p->n == 0) {
                    prev->next = p->next;
                    free(p->values);
                    free(p->key);
                    free(p);
                }
                else {
                    for (; i < p->n; i++)
                        p->values[i] = p->values[i + 1];
                }
            }

            pthread_mutex_unlock(&hs->mutex);
            return;
        }
        prev = p;
    }
    pthread_mutex_unlock(&hs->mutex);
}

void hs_delete_all(hs_t hs, int value) {
    node *p, *prev;
    unsigned int i;

    for (int h = 0; h < HS_SIZE; h++) {
        prev = p = (node*) &hs[h];
        pthread_mutex_lock(&hs[h].mutex);

        while ((p = p->next)) {
            for (i = 0; i < p->n; i++) {
                if (p->values[i] == value) {
                    p->n--;

                    if (p->n == 0) {
                        prev->next = p->next;
                        free(p->key);
                        free(p->values);
                        free(p);
                        p = prev;
                        break;
                    }
                    else {
                        for (; i < p->n; i++)
                            p->values[i] = p->values[i + 1];
                    }
                }
            }
            prev = p;
        }
        pthread_mutex_unlock(&hs[h].mutex);
    }
}
