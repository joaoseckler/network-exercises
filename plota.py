import numpy as np
import matplotlib.pyplot as plt


cpu_media0     = 0.002
cpu_media2     = 4.621
cpu_media100   = 5.674
cpu_desvp0     = 0.001154700538
cpu_desvp2     = 0.5983765073
cpu_desvp100   = 2.715339962

rede_media0    = 0
rede_media2    = 50255053.8
rede_media100  = 1485571.2
rede_desvp0    = 0
rede_desvp2    = 6939279.331
rede_desvp100  = 377426.6392

fig, ax = plt.subplots()
ax.bar([0, 1, 2], [cpu_media0, cpu_media2, cpu_media100], yerr=[cpu_desvp0,
    cpu_desvp2, cpu_desvp100], align='center', alpha=0.5, ecolor='black', capsize=10)
ax.set_ylabel('Uso da CPU (s)')
ax.set_xticks([0, 1, 2])
ax.set_xticklabels(['0 clientes', '2 clientes', '100 clientes'])
ax.set_title('')
ax.yaxis.grid(True)

plt.tight_layout()
plt.savefig('cpu.png')

fig, ax = plt.subplots()
ax.bar([0, 1, 2], [rede_media0, rede_media2, rede_media100], yerr=[rede_desvp0,
    rede_desvp2, rede_desvp100], align='center', alpha=0.5, ecolor='black', capsize=10)
ax.set_ylabel('Uso da rede (bytes)')
ax.set_xticks([0, 1, 2])
ax.set_xticklabels(['0 clientes', '2 clientes', '100 clientes'])
ax.set_title('')
ax.yaxis.grid(True)

plt.tight_layout()
plt.savefig('rede.png')
