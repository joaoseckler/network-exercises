#include "packet.h"

uint8_t *mk_suback_packet(uint16_t identifier, uint64_t *size) {
    uint8_t *packet, *p;
    p = packet = malloc(6 * sizeof(uint8_t));
    *p++ = 0x90; // fixed header
    *p++ = 0x04; // remaining length
    memcpy(p, &identifier, 2 * sizeof(uint8_t)); //identifier
    p += 2;
    *p++ = 0x00; // Properties
    *p = 0x00; // Reason code

    *size = 6;
    return packet;
}


uint8_t *mk_subscribe_packet(char **topics, uint64_t *size) {
    uint32_t topic_l, topic_n = 0, remaining_l, tmp;
    uint8_t *packet, *p, *q;
    char **t = topics, *s;

    remaining_l = 3; // 2 for identifier and 1 for properties

    while (*t) {
        topic_n++; // Count numer of topics
        remaining_l += strlen(*t) + 2 + 1; // 2: topic size, 1: subscr options
        t++;
    }
    *size = remaining_l + 1;

    tmp = remaining_l;
    while (tmp > 0) { // calculate size of remaining_length
        (*size)++;
        tmp >>= 7;
    }

    p = packet = calloc(*size, sizeof(uint8_t));
    *p++ = 0x82;
    p = encode_vbi(p, remaining_l);
    *p++ = 0;
    *p++ = 1; // identifier
    *p++ = 0; // properties

    t = topics;
    while (*t) {
        s = *t; // s: current topic
        q = p + 2;
        topic_l = 0;
        while (*s) {
            *q++ = *s++;
            topic_l++;
        }
        *q++ = 0; // subscr. options for this topic
        *p++ = (uint8_t) topic_l >> 8;
        *p = (uint8_t) topic_l;
        p = q;
        t++;
    }

    return packet;
}

uint8_t *mk_publish_packet(char *topic, char *message, uint64_t *size) {
    uint8_t *packet, *p;
    uint32_t remaining_l, tmp;
    uint16_t topic_l;

    topic_l = strlen(topic);
    // + 2 for the size of topic, +1 for properties
    remaining_l = topic_l + strlen(message) + 2 + 1;
    // initial header + topic size
    *size = 1 + remaining_l;

    tmp = remaining_l;
    while (tmp > 0) { // calculate size of remaining_length
        (*size)++;
        tmp >>= 7;
    }

    p = packet = calloc(*size, sizeof(uint8_t));

    *p++ = 0x30;
    p = encode_vbi(p, remaining_l);
    *p++ = (uint8_t) topic_l >> 8;
    *p++ = (uint8_t) topic_l;

    while (*topic != '\0')
        *p++ = *topic++;
    *p++ = 0; // Properties
    while (*message != '\0')
        *p++ = *message++;

    return packet;
}

void parse_publish_packet(uint8_t *packet, char **topic, char **message) {
    uint32_t message_l, remaining_l, properties_l;
    uint16_t topic_l;
    uint8_t *p = packet, *end;

    if (!packet || packet[0] >> 4 != PUBLISH) {
        fprintf(stderr, "parse_publish: packet malformed");
        return;
    }
    // DUP, QoS and Retain ignored
    p++;
    p = decode_vbi(p, &remaining_l);
    end = p + remaining_l;
    topic_l = *p++ << 8;
    topic_l += *p++;
    *topic = malloc(sizeof(char) * (topic_l + 1));
    /* memcpy(*topic, p, (size_t) topic_l); */
    for (uint16_t i = 0; i < topic_l; i++)
      (*topic)[i] = *p++;
    (*topic)[topic_l] = '\0';

    if ((packet[0] & 6) != 0) // If QoS is set, skip topic identifier
        p += 2;

    p = decode_vbi(p, &properties_l); // Properties...
    p += properties_l; // ... ignore them

    message_l = end - p;
    *message = malloc(sizeof(char) * (message_l + 1));

    for (uint16_t i = 0; i < message_l; i++)
      (*message)[i] = *p++;
    (*message)[message_l] = '\0';
}

char ** parse_subscribe_packet(uint8_t *packet, uint16_t *identifier) {
    // Maybe: reject wildcards as stated in the protocol
    uint32_t topic_l, topic_n = 0, remaining_l, properties_l, i = 0;
    uint8_t *p = packet, *q, *end;
    char **topics;

    if (!packet || packet[0] >> 4 != SUBSCRIBE) { fprintf(stderr, "parse_publish: packet malformed");
        return NULL;
    }
    p++;
    p = decode_vbi(p, &remaining_l);
    end = p + remaining_l;

    *identifier = *p++ << 8;
    *identifier = *p++;

    p = decode_vbi(p, &properties_l); // Properties...
    p += properties_l; // ... ignore them

    q = p;
    while (q < end) { // Count numer of topics
        topic_n++;
        topic_l = *q++ << 8;
        topic_l += *q++;
        q += topic_l;
        q += 1; // topic subscription options
    }

    topics = calloc(topic_n + 1, sizeof(char*));
    topics[topic_n] = NULL;

    while (p < end) {
        topic_l = *p++ << 8;
        topic_l = *p++;
        topics[i] = malloc(sizeof(char) * (topic_l + 1));
        memcpy(topics[i], p, topic_l);
        topics[i][topic_l] = '\0';
        p += topic_l;
        p++; // ignoring requested QoS
        i++;
    }

    return topics;
}

uint8_t * decode_vbi(uint8_t * encoded, uint32_t *value) {
    // Código sugerido na especificação do protocolo MQTT
    // https://docs.oasis-open.org/mqtt/mqtt/v5.0/os/mqtt-v5.0-os.html
    uint multiplier = 1;
    *value = 0;
    encoded--;

    do {
       encoded++;
       *value += (*encoded & 127) * multiplier;
       if (multiplier > 128*128*128) { // Maximum is four
           fprintf(stderr, "decode_vbi: Malformed Variable Byte Integer\n");
           return encoded;
       }
       multiplier *= 128;
    } while ((*encoded & 128) != 0);

    return encoded + 1;
}

uint8_t * encode_vbi(uint8_t *p, uint32_t value) {
    // Código sugerido na especificação do protocolo MQTT v3.1.1 seção 2.2.3
    // https://docs.oasis-open.org/mqtt/mqtt/v3.1.1/os/mqtt-v3.1.1-os.html
    uint8_t byte;
    do {
        byte = value % 128;
        value /= 128;
        // if there are more data to encode, set the top bit of this byte
        if (value > 0)
            byte |= 128;

        *p++ = byte;
    } while (value > 0);
    return p;
}
