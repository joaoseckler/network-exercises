#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>

enum packet {
    RESERVED,
    CONNECT,
    CONNACK,
    PUBLISH,
    PUBACK,
    PUBREC,
    PUBREL,
    PUBCOMP,
    SUBSCRIBE,
    SUBACK,
    UNSUBSCRIBE,
    UNSUBACK,
    PINGREQ,
    PINGRESP,
    DISCONNECT,
    AUTH
};

uint8_t *mk_publish_packet(char *topic, char *message, uint64_t *size);
uint8_t *mk_subscribe_packet(char **topics, uint64_t *size);
void parse_publish_packet(uint8_t *packet, char **topic, char **message);
char ** parse_subscribe_packet(uint8_t *packet, uint16_t *identifier);
uint8_t * decode_vbi(uint8_t * encoded, uint32_t *value);
uint8_t * encode_vbi(uint8_t *p, uint32_t value);
uint8_t *mk_suback_packet(uint16_t identifier, uint64_t *size);
