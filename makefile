CFLAGS=-Wall -pedantic -Wextra -pthread -g

all: ep1

ep1: ep1.c packet.o hs.o packet.h hs.h

tags: *.c *.h
	ctags -R

test: test.c packet.o hs.o packet.h hs.h

client: packet.o

clean:
	rm -rf *.o ep1 test tags client
