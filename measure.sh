#/bin/bash

reps="$2"
port="$3"

if [ $# -ne 3 ] || [ -n "${reps##[0-9]*}" ]; then
  echo passe o nome do logo como primeiro argumento, por favor
  echo e o segundo o número de clientes
  echo e o terceiro a porta
  exit 1
fi

sleep=20

./traffic.sh $((sleep + 1)) "$1".txt  $port > $$.txt &

sleep 0.5;
echo vou começar o servidor. Porta $port, out $1.txt:
(/usr/bin/time --format '%U %S' ./ep1 $port) >/dev/null 2> "$1".txt &

sleep 0.5;
for ((i = 0; i < $reps; i++)); do
  echo -e '\t'vou começar o $i-ésimo cliente. Porta $port
  ./client $port > /dev/null &
done

sleep $((sleep - 1))
pkill -x client
pkill -x ep1

cat "$$.txt" >> "$1".txt
rm $$.txt
